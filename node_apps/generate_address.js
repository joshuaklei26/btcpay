var http = require("http");
var restify = require('restify');
var bitcoin = require("bitcoinjs-lib");

var testnet = bitcoin.networks.testnet
var mainnet = bitcoin.networks.mainnet

function testnet_address(req, res, next) {
    res.setHeader("Content-type" , "application/json" );
    var keyPair = bitcoin.ECPair.makeRandom({network: testnet});
    var address = bitcoin.payments.p2pkh({ pubkey : keyPair.publicKey, network: testnet }).address;
    var public_key = keyPair.publicKey.toString("hex")
    var private_key = keyPair.toWIF();
    res.json({ address: address, public_key: public_key, private_key: private_key });
    next();
}

function mainnet_address(req, res, next) {
    res.setHeader( "Content-type" , "application/json");
    var keyPair = bitcoin.ECPair.makeRandom({network: mainnet});
    var address = bitcoin.payments.p2pkh({ pubkey : keyPair.publicKey, network: mainnet }).address;
    var public_key = keyPair.publicKey.toString("hex")
    var private_key = keyPair.toWIF();
    res.json({ address: address, public_key: public_key, private_key: private_key });
    next();
}

var generate_address = restify.createServer();
generate_address.get('/generate_testnet_address', testnet_address);
generate_address.get('/generate_mainnet_address', mainnet_address);

generate_address.listen(8081, function() {
    console.log('%s listening at %s', generate_address.name, generate_address.url);
});

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import MySQLdb
import common_util

# Create your models here.

def get_db_connect(logger):
    """
    MySQL Connection
    :return: DB object
    """
    try:
        db = MySQLdb.connect(
            host = common_util.config.get('sql', 'host'),
            user = common_util.config.get('sql', 'user'),
            passwd = common_util.config.get('sql', 'passwd'),
            db = common_util.config.get('sql', 'db')
        )
        return db
    except Exception as e:
        print 'get_db_connect :%s'%(str(e))
        logger.msg_logger("Error get_db_connect : " + str(e))
        raise Exception(str(e))


def insert_sql(logger, table_name, data):
    try:
        ret_status = False
        db = get_db_connect(logger)
        cursor = db.cursor()
        query = 'insert into %s(%s) values(%s)' % (table_name, ','.join([key for key in data]),','.join(['%s' for _ in data]))
        values = tuple([value for key,value in data.items()])
        cursor.execute(query,(values))
        db.commit()
        logger.msg_logger('>>>>>>>> MYSQL Insert Success : %s || %s' % (query, str(data)))
        ret_status = True
    except Exception as e:
        print 'insert_sql :%s'%(str(e))
        logger.msg_logger('Error insert_sql : %s | %s'%(str(e),query))
    finally:
        if db : db.close()
        return ret_status


def find_sql(logger, table_name,filters,columns=''):
    try:
        data = None
        db = get_db_connect(logger)
        cursor = db.cursor(MySQLdb.cursors.DictCursor)

        if columns:
            columns = ','.join(columns)
        else:
            columns = '*'

        params = ''
        for key,value in filters.items():
            params += "%s = '%s' AND "%(key,value)
        params = params[:-5] # Removing AND

        query = 'SELECT %s FROM %s WHERE %s'%(columns,table_name,params)
        cursor.execute(query)
        data = cursor.fetchall()
    except Exception as e:
        print 'find_sql :%s'%(str(e))
        logger.msg_logger('find_sql : %s | %s'%(str(e),query))
    finally:
        if db: db.close()
        return data

def query_from_filter(filters, type='AND'):
    params = ''
    for key, value in filters.items():
        params += "%s = '%s' %s " % (key, value, type)
    return params[:-(len(type)+2)]


def update_sql(logger, table_name, filters, updated_values):
    """
    Custom method for Update SQL Query
    :param logger:
    :param table_name:
    :param filters:
    :param updated_values:
    :return:
    """
    try:
        ret_status = False
        db = get_db_connect(logger)
        cursor = db.cursor()
        update_params = query_from_filter(updated_values,type=',')
        filter_params = query_from_filter(filters)

        # Check if present
        if find_sql(logger, table_name=table_name, filters=filters) :
            query = 'UPDATE %s SET %s WHERE %s' % (table_name, update_params, filter_params)
            cursor.execute(query)
            db.commit()
            logger.msg_logger('>>>>>>>> MYSQL update Success : %s' % (query))
            ret_status = True

    except Exception as e:
        print e
        logger.error_logger('update_sql : %s || %s'%(str(e),query))
    finally:
        if db : db.close()
        return ret_status
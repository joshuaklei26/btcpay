#!/bin/bash
echo "**********************************************************"
echo "*************Creating log directories.....****************"
echo "**********************************************************"
echo "*************Creating apache logs directory.....*************"
mkdir /var/www/logs
echo "**********************************************************"
echo "*************Creating btc_logs directory.....*************"
mkdir /var/log/btc_logs
echo "**********************************************************"
echo "*************Creating end_points directory.....***********"
mkdir /var/log/btc_logs/end_points
echo "**********************************************************"
echo "*************Creating mempool directory.....**************"
mkdir /var/log/btc_logs/mempool
echo "**********************************************************"
echo "*************Creating block directory.....****************"
mkdir /var/log/btc_logs/block
echo "**********************************************************"
echo "*************Creating hook_main directory.....************"
mkdir /var/log/btc_logs/hook_main
echo "**********************************************************"
echo "*************Setting write permissions.....***************"
cd /var/log
chmod 777 btc_logs
cd btc_logs
chmod 777 /var/log/btc_logs/end_points
chmod 777 /var/log/btc_logs/mempool
chmod 777 /var/log/btc_logs/block
chmod 777 /var/log/btc_logs/hook_main
cd /var/www
chmod 777 logs
echo "**********************************************************"
echo "*************DONE!!***************************************"
echo "**********************************************************"
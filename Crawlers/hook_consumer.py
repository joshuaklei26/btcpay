import sys, os
import pika
import requests
import json
import datetime
from util import MyLogger,config,send_notification,insert_sql

logs_directory, category, headers = None,None,None

def callback(ch, method, properties, body):
    """
    This method is called every time there is a new element in queue (var : queue)
    :param ch:
    :param method:
    :param properties:
    :param body:
    :return:
    """
    try:

        # Logs
        print 'In Hook Consumer'
        obj_logger = MyLogger(logs_directory, category)
        obj_logger.msg_logger('Getting Data : %s'%(datetime.datetime.now()))

        # Data from Queue
        data = json.loads(body)
        notification_url = data['notification_url']
        data.pop('notification_url')
        notification_params = data

        obj_logger.msg_logger('>>>>>>>>>> Sending Notification : %s || %s' % (notification_url, notification_params))
        # Send Notification
        send_request = requests.post(notification_url, data=json.dumps(notification_params), headers=headers)
        if send_request.status_code == 200:
            obj_logger.msg_logger(
                '>>>>>>>>>> Notification Success : %s || %s' % (notification_url, notification_params))

            # Insert in DB
            insert_sql(logger=obj_logger, table_name='notification_logs', data={
                'tx_hash': notification_params['tx_id'],
                'notification_url ': notification_url,
                'params': str(notification_params),
                'timestamp': datetime.datetime.now(),
                'Status': 'Success'
            })

            # ACK on Success
            ch.basic_ack(delivery_tag=method.delivery_tag)
        else:
            send_request.raise_for_status()

    except Exception as e:
        print 'Exception in Hook Consumer : ' + str(e)
        obj_logger.error_logger('>>>>>>>>>> Notification Failure : %s || %s || %s' % (e, notification_url, notification_params))

def main():
    try :
        global logs_directory, category, headers
        headers = {'Content-type': 'application/json'}
        logs_directory = config.get('hook_main', 'logs')
        category = config.get('hook_main', 'category')

        # Connection
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        # Queue
        queue = config.get('hook_main', 'queue')
        channel.queue_declare(queue=queue,durable=True)
        channel.basic_consume(callback,queue=queue)
        channel.start_consuming()

    except pika.exceptions.ConnectionClosed:
        e = 'Rabbit MQ is Down'
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print  error_stack
        obj_logger = MyLogger(logs_directory, category)
        obj_logger.error_logger('Main : %s' % (error_stack))

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print  error_stack
        obj_logger = MyLogger(logs_directory, category)
        obj_logger.error_logger('Main : %s' % (error_stack))


if __name__ == '__main__':
    main()